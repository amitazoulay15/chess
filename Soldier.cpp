#include "Soldier.h"

Soldier::Soldier(Point p, char tAc)
{
	this->_loc = p;
	this->_typeAndColor = tAc;
	this->_num_of_moves = 0;
}

Soldier::~Soldier()
{
	
}

Soldier::Soldier()
{
	this->_loc = Point();
	this->_typeAndColor = HASHTAG;
}

char Soldier::getTypeAndColor()
{
	return this == nullptr ? HASHTAG : this->_typeAndColor;
}

Point& Soldier::getPoint()
{
	return this->_loc;
}

Soldier& Soldier::operator=(const Soldier& other)
{
	if (this == &other)
	{
		return *this;
	}
	this->_loc = other._loc;
	this->_typeAndColor = other._typeAndColor;
	return *this;
}

void Soldier::incNumOfMoves()
{
	_num_of_moves++;
}

Soldier* Soldier::getRun()
{
	return nullptr;
}

Soldier* Soldier::getTower()
{
	return nullptr;
}

char Soldier::getColor()
{
	if (this->getTypeAndColor() >= 'A' && this->getTypeAndColor() < 'a')
	{
		return WHITE;
	}
	else if (this->getTypeAndColor() > 'a')
	{
		return BLACK;
	}
	return HASHTAG;
}

