#pragma once
#include "Soldier.h"

class Horse : public Soldier
{
public:
	Horse(Point p, char tAc);
	~Horse();
	bool move(Point& x, Board& b) override;
	bool checkSolOnTheWay(Point& x, Board& b) override;
};


