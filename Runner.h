#pragma once
#include "Soldier.h"

class Runner : virtual public Soldier
{
public:
	Runner(Point p, char tAc);
	~Runner();
	virtual bool move(Point& x, Board& b) override;
	virtual bool checkSolOnTheWay(Point& x, Board& b) override;
};

