#include "Board.h"
#include "Hir.h"
#include "Horse.h"
#include "King.h"
#include "Queen.h"

Board::Board(std::string s)
{
	int c = 0;
	Soldier* sol = nullptr;
	for (int i = 0; i < THE_MATRIX; i++)
	{
		for (int j = 0; j < THE_MATRIX; j++)
		{
			if (s[c] == 'r' || s[c] == 'R')
			{
				sol = new Tower(Point(j, i), s[c]);
			}
			else if (s[c] == 'k' || s[c] == 'K')
			{
				sol = new King(Point(j, i), s[c]);
			}
			else if (s[c] == 'n' || s[c] == 'N')
			{
				sol = new Horse(Point(j, i), s[c]);
			}
			else if (s[c] == 'b' || s[c] == 'B')
			{
				sol = new Runner(Point(j, i), s[c]);
			}
			else if (s[c] == 'q' || s[c] == 'Q')
			{
				sol = new Queen(Point(j, i), s[c]);
			}
			else if (s[c] == 'p' || s[c] == 'P')
			{
				sol = new Hir(Point(j, i), s[c]);
			}
			else
			{
				sol = nullptr;
			}
			this->_board[i][j] = sol;
			c++;
		}
	}
}

Board::~Board()
{
	char c;
	for (int i = 0; i < THE_MATRIX; i++)
	{
		for (int j = 0; j < THE_MATRIX; j++)
		{
			if (this->_board[i][j] != nullptr)
			{
				c = this->_board[i][j]->getTypeAndColor();
				/*if (this->_board[i][j]->getRun() != nullptr)
				{
					delete this->_board[i][j]->getRun(); when i try to do that there is an error
					delete this->_board[i][j]->getTower(); when i try to do that there is an error
				}*/
				if (c != 'B' && c != 'b' && c != 'R' && c != 'r' && c != HASHTAG)
					delete this->_board[i][j];
			}
		}
	}
}

int Board::checkMove(Point loc, Point newLoc, const char turn)
{
	Soldier* s, * dest;
	s = this->_board[loc.getY()][loc.getX()];
	dest = this->_board[newLoc.getY()][newLoc.getX()];
	if ((s->getColor() == BLACK && turn == WHITE) || (s->getColor() == WHITE && turn == BLACK))//the selected paw is not the player's paw
	{
		return 2;
	}
	else if ((dest->getColor() == WHITE && turn == WHITE) || (dest->getColor() == BLACK && turn == BLACK))// if the dest has the same color paw
	{
		return 3;
	}
	if (s->move(newLoc, *this))//move of player is valid
	{
		this->moveSol(loc, newLoc);
		if (this->checkChes(turn))
		{
			this->moveSol(newLoc, loc);
			return 4;
		}
	}
	else//the move is not valid
	{
		return 6;
	}
	this->_board[newLoc.getY()][newLoc.getX()]->incNumOfMoves();
	//need to check chess
	if (turn == WHITE)
	{
		return this->checkChes(BLACK) ? 1 : 0;
	}
	return this->checkChes(WHITE) ? 1 : 0;
}

void Board::boardToString(char * toRet)
{
	int c = 0;
	for (int i = THE_MATRIX - 1; i >= 0; i--)
	{
		for (int j = 0; j < THE_MATRIX; j++)
		{
			toRet[c] = this->_board[i][j]->getTypeAndColor();
			c++;
		}
	}
	toRet[64] = '0';
}

bool Board::checkChes(const char color)
{
	Point loc;
	Soldier* s;
	loc = this->getKingLoc(color);
	for (int i = 0; i < THE_MATRIX; i++)
	{
		for (int j = 0; j < THE_MATRIX; j++)
		{
			s = this->_board[i][j];
			if (((s->getColor() == BLACK && color == WHITE) || (s->getColor() == WHITE && color == BLACK)) && s->move(loc, *this))
			{
				return true;
			}
		}
	}
	return false;
}

Point Board::getKingLoc(const char color)
{
	Soldier* s;
	for (int i = 0; i < THE_MATRIX; i++)
	{
		for (int j = 0; j < THE_MATRIX; j++)
		{
			s = this->_board[i][j];
			if ((s->getTypeAndColor() == 'K' && color == WHITE) || (s->getTypeAndColor() == 'k' && color == BLACK))
			{
				return this->_board[i][j]->getPoint();
			}
		}
	}
	return Point();
}

void Board::moveSol(Point loc, Point newLoc)
{
	char c;
	c = this->_board[newLoc.getY()][newLoc.getX()]->getTypeAndColor();
	if (c != HASHTAG)
	{
		/*if (this->_board[newLoc.getY()][newLoc.getX()]->getRun() != nullptr)
		{
			delete this->_board[newLoc.getY()][newLoc.getX()]->getRun(); //when i try to do that there is an error
			delete this->_board[newLoc.getY()][newLoc.getX()]->getTower(); //when i try to do that there is an error
		}*/
		if (c != 'B' && c!= 'b' && c != 'R' && c != 'r')
			delete this->_board[newLoc.getY()][newLoc.getX()];
	}
	this->_board[newLoc.getY()][newLoc.getX()] = this->_board[loc.getY()][loc.getX()];
	this->_board[newLoc.getY()][newLoc.getX()]->getPoint() = newLoc;
	if (this->_board[newLoc.getY()][newLoc.getX()]->getRun() != nullptr)
	{
		this->_board[newLoc.getY()][newLoc.getX()]->getRun()->getPoint() = newLoc;
		this->_board[newLoc.getY()][newLoc.getX()]->getTower()->getPoint() = newLoc;
	}
	this->_board[loc.getY()][loc.getX()] = nullptr;
}

char Board::getSolInLoc(Point* loc)
{
	char ret = this->_board[loc->getY()][loc->getX()]->getTypeAndColor();
	delete loc;
	return ret;
}

void Board::printBoard()
{
	for (int i = THE_MATRIX - 1; i >= 0; i--)
	{
		std::cout << (char)(i + '0' + 1) << " ";
		for (int j = 0; j < THE_MATRIX; j++)
		{
			std::cout << this->_board[i][j]->getTypeAndColor() << " ";
		}
		std::cout << "\n";
	}
	std::cout << "  a b c d e f g h\n";
}

