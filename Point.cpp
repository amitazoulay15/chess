#include "Point.h"
#include <cstddef>

Point::Point()
{
	_x = NULL;
	_y = NULL;
}

Point::Point(const int x, const int y)
{
	_x = x;
	_y = y;
}

Point::~Point()
{
}

Point& Point::operator=(const Point& other)
{
	if (this == &other)
	{
		return *this;
	}
	this->_x = other._x;
	this->_y = other._y;
	return *this;
}

bool Point::operator==(const Point& other) const
{
	if (this->_x == other._x && this->_y == other._y)
	{
		return true;
	}
	return false;
}

int Point::getX()
{
	return this->_x;
}

int Point::getY()
{
	return this->_y;
}
