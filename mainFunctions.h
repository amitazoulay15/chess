#pragma once
#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Board.h"


/*
this method is the function that convertes the entered string into 2 points
input: 2 points & and a string
output:
*/
void string2TwoPoint(Point& p1, Point& p2, const std::string s);