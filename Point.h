#pragma once

class Point
{
public:
	Point(); 
	/*
	this method is a c'tor of the Point class
	input: x value, y value
	output: non
	*/
	Point(const int x, const int y);
	~Point();
	/*
	this method is the operator '=' for the class Point
	input: another Point class object
	output: &to the object i coppied to
	*/
	Point& operator=(const Point& other);
	/*
	this method is the operator '=='
	input: Point class object
	output: bool - if the 2 objects have the same values
	*/
	bool operator==(const Point& other) const;
	/*
	this method gets the x value
	input: non
	output: int
	*/
	int getX();
	/*
	this method gets the y value
	input: non
	output: int
	*/
	int getY();
protected:
	int _x;
	int _y;
};