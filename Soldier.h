#pragma once
#include "Point.h"
#include <iostream>
#include "Board.h"

class Board;

class Soldier
{
public:
	/*
	this method is the c'tor of the class
	input: point to set at, the cheracter of the soldier
	output: non
	*/
	Soldier(Point p, char tAc);
	~Soldier();
	Soldier();
	/*
	this method is a pure virtual method that checks if the move can be done
	input: point to move the object to, the board to move the soldier on
	output: bool - if the player has been move true, false otherwise
	*/
	virtual bool move(Point& x, Board& b) = 0;
	/*
	this method is a pure virtual methods that checks if there are any soldiers on the way between the corrent soldier to the asked sqr
	input: the asked sqr, the board
	output: bool - if there is a soldoer between them rturns true, false otherwise
	*/
	virtual bool checkSolOnTheWay(Point& x, Board& b) = 0;
	/*
	this method returns the type and color of the soldier
	input: non
	output: char
	*/
	char getTypeAndColor();
	/*
	this method returns the point of the soldier
	input: non
	output: Point&
	*/
	Point& getPoint();
	/*
	this method is an operator the '=' operator for the soldier class
	input: other object from class Soldier
	output: &to the object we coppied to
	*/
	Soldier& operator=(const Soldier& other);
	/*
	this method is increacing the number of moves a soldier has made
	input: non
	output: non
	*/
	void incNumOfMoves();
	virtual Soldier* getRun();
	virtual Soldier* getTower();
	/*
	this function checks the color of the pawn
	input: non
	outpuy: 'w' if the pawn is white, 'b' if the pawn is black '#' otherwise
	*/
	char getColor();
protected:
	char _typeAndColor;
	Point _loc;
	int _num_of_moves;
};