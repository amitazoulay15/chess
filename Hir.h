#pragma once
#include "Soldier.h"

class Hir : public Soldier
{
public: 
	Hir(Point p, char tAc);
	~Hir();
	bool move(Point& x, Board& b) override;
	bool checkSolOnTheWay(Point& x, Board& b) override;
};

