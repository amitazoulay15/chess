#pragma once
#include <iostream>
#include "Soldier.h"

#define THE_MATRIX 8
#define	WHITE 'w'
#define BLACK 'b'
#define HASHTAG '#'

class Soldier;

class Board
{
public:
	/*
	this method is the c'tor of the board object
	input: a string to make the soldiers from
	output: non
	*/
	Board(std::string s);
	~Board();
	/*
	this method checks the move code
	input: start point wanted point and the wich player's turn is it
	output: int - the move code
	*/
	int checkMove(Point loc, Point newLoc, const char turn);
	/*
	this method converts the board to a string that the FE can read
	input: an array of chars to put the string in
	output: non
	*/
	void boardToString(char* toRet);
	/*
	this method checks if there is a chess on the asked player
	input: the color of the asked player
	output: bool - true if there is chess, false otherwise
	*/
	bool checkChes(const char color);
	/*
	this method gets the king location
	input: the color of the asked king
	output: the Point of the king
	*/
	Point getKingLoc(const char color);
	/*
	this method is moooving the soldier in the board
	input: the location of the soldier, the wanted location for the soldier
	output: non
	*/
	void moveSol(Point loc, Point newLoc);
	/*
	this method gets the soldier in a point in the board
	input: a location
	output: the type and color of the soldier in the board
	*/
	char getSolInLoc(Point* loc);
	/*
	this method prints the board (used it for myself not important to the game)
	input: non
	output: non
	*/
	void printBoard();
protected:
	Soldier* _board[THE_MATRIX][THE_MATRIX];
};