#pragma once
#include "Soldier.h"

class Tower : virtual public Soldier
{
public:
	Tower(Point p, char tAc);
	~Tower();
	virtual bool move(Point& x, Board& b) override;
	virtual bool checkSolOnTheWay(Point& x, Board& b) override;
};

